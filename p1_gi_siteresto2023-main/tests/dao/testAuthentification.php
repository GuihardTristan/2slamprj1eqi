<?php

use modele\dao\Bdd;
use modele\dao\UtilisateurDAO;


require_once '../../includes/autoload.inc.php';
require_once "../../includes/authentification.inc.php";       // gestion des sessions authentifiées

Bdd::connecter();
// test d'authentification applicative
//login("mathieu.capliez@gmail.com", "Passe1?");
$mailU = "matheo@gmail.com";
echo "Tentative de connexion de $mailU<br/>";
login($mailU, "secret");
if (isLoggedOn()) {
    echo "Ok, $mailU is logged<br/>";
} else {
    echo "Problème, $mailU is not logged<br/>";
}
if (!isset($_SESSION)) {
        session_start();
    }
    // Rechercher les données relatives à cet utilisateur
    $util = UtilisateurDAO::getOneByMail($mailU);
    echo $util->getMailU(),"<br/>";
    $mdpU = "secret";
    // Si l'utilisateur est connu (mail trouvé dans la BDD)
    if (!is_null($util)) {
        $mdpBD = $util->getMdpU();
        $idU = $util->getIdU();
        echo $idU,"<br/>";
        echo $mdpBD,"<br/>";
        
        

        // Si le mot de passe saisi correspond au mot de passe "haché" de la BDD
        if (password_verify($mdpU, $mdpBD)) {
            // le mot de passe est celui de l'utilisateur dans la base de donnees
            $_SESSION["idU"] = $idU;        // la clef est idU désormais
            $_SESSION["mailU"] = $mailU;
            $_SESSION["mdpU"] = $mdpBD;
            echo $_SESSION["mdpU"];
        }else{
            echo "ça ne marche pas<br/>";
        }
    }


// deconnexion
echo "Tentative de déconnexion<br/>";
logout();
if (isLoggedOn()) {
    echo "Problème, $mailU is always logged<br/>";
} else {
    echo "Ok, $mailU is no more logged<br/>";
}


echo "Tentative de connexion avec un login inexistant<br/>";
$mailU = "inconnu@bts.sio";
login($mailU, "1234");
if (isLoggedOn()) {
    echo "Problème, $mailU ne devrait pas être connecté<br/>";
} else {
    echo "Ok, $mailU n'est pas connecté<br/>";
}



